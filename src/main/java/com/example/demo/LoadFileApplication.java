package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(StoageProperties.class)
public class LoadFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoadFileApplication.class, args);
	}
	@Bean
	CommandLineRunner init(StorageService storageService)
}
